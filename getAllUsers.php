<?php
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website

// Connect
$link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$sql = "SELECT FIRST_NAME, LAST_NAME FROM users";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        echo $row["FIRST_NAME"]. " " . $row["LAST_NAME"]. "<br>";
    }
}
else {
    echo "0 results";
}


// Close connection
mysqli_close($link);
?>