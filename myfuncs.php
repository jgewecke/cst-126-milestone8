<style>
<?php require_once('style.css'); 
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp?>
</style>

<?php

function dbConnect() {
    // Connect to azure
    $link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");
    
    // Connect to local
    //$link = mysqli_connect("127.0.0.1", root, root, "activity1");
    
    // Check connection
    if($link === false){
        die("ERROR (myfuncs.php): Could not connect. " . mysqli_connect_error());
    }
    return $link;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
    $_SESSION["POST_ID"] = 0;
}
function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}

function forgetUserId()
{
    session_start();
    $_SESSION = array();
}

// Uses session to automatically get userID
function getUserArrayFromCurrentUser($link)
{
    session_start();
    $userID = $_SESSION["USER_ID"];
    
    $sql = "SELECT * FROM users WHERE ID='$userID'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows >= 1)
    {
        return $result->fetch_assoc();	// Read the Row from the Query
    }
    else
    {
        return null;
    }
}

function getPostFromID($link, $id)
{   
    $sql = "SELECT * FROM posts WHERE ID='$id'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if ($numRows >= 1)
    {
        return $result->fetch_assoc();	// Read the Row from the Query
    }
    else
    {
        echo('(myfuncs.php) No post found in database');
        return null;
    }
}

function getUsersByFirstName($link, $pattern)
{
    $sql = "SELECT * FROM users WHERE FIRST_NAME LIKE '$pattern'";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);
    if (mysqli_num_rows($result) > 0) {
        $index = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);

            ++$index;
        }

        return $users;
    }
    else {
        return null;
    }
}

function getAllUsers($link)
{
    // Check connection
    if($link === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    $sql = "SELECT FIRST_NAME, LAST_NAME FROM users";
    $result = mysqli_query($link, $sql);

    if (mysqli_num_rows($result) > 0) {
        $index = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $users[$index] = array($row["ID"], $row["FIRST_NAME"], $row["LAST_NAME"]);

            ++$index;
        }

        return $users;
    }
    else {
        return null;
    }
}

function get_posts($link, $sql)
{
    $result = mysqli_query($link, $sql);
    $currentUser = getUserId();
    $numRows = mysqli_num_rows($result);

    if ($numRows >= 1)
    {
        foreach($result as $index => $i) {
            $sql = "SELECT ID, AUTHOR_ID, TITLE, TEXT FROM posts"; 
            
            $postID = $i["ID"];
            $text = $i["TEXT"];
            $title = $i["TITLE"];
            $authorID = $i["AUTHOR_ID"];
            $tag = $i["TAG"];
            $rating = $i["RATING"];
    
            // Get First and Last name of Author from AUTHOR_ID
            $sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE ID='$authorID'";
            $i = mysqli_query($link, $sql);
            $row = $i->fetch_assoc();	// Read the Row from the Query
            $author = $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"];
            
            // Get user rating preference for this post (if any)
            $sql = "SELECT * FROM ratings WHERE USER_ID='$currentUser' AND POST_ID='$postID'";
            $result = mysqli_query($link, $sql);
            $rating_row = $result->fetch_assoc();	// Read the Row from the Query
            $users_rating = $rating_row["RATING"];
            if ($users_rating == 1) $users_rating = '+1';
            if ($users_rating == -1) $users_rating = '-1';
            if ($users_rating == 0) $users_rating = '0';

            // Display result
             echo ("<h1>$title</h1> <h2>by $author</h2> <p>$text</p> <p id='tag'>Tag: $tag</p> <div class='rating'> <p>Rating:&nbsp;</p> <p id='$index'>$rating</p> <p>&nbsp;($users_rating)</p> </div>");

            // Display comment(s) for post
            $sql = "SELECT * FROM comments WHERE POST_ID='$postID'";
            $result = mysqli_query($link, $sql);
            $numCommentRows = mysqli_num_rows($result);

            echo ("<h3>Comments:</h3>");
            if ($numCommentRows > 0)
            {
                foreach($result as $b)
                {
                    $poster_name = $b["FIRST_NAME"] . ' ' . $b["LAST_NAME"];
                    $text = $b["TEXT"];
                    $date = $b["SUBMIT_DATE"];
        
                    echo ('<div class="comment">');
                    echo ("<h3>$poster_name</h3> <p>&nbsp;&nbsp;&nbsp;&nbsp;$text</p> <p>Submit Date: $date</p>");
                    echo (' </div>');
                }
            }
            else
            {
                echo ("This post has no comments yet.");
            }

            // Upvote/Downvote buttons
            echo('<form action="ratingHandler.php" method="POST">');
            echo('<button type="submit" formmethod="post" class="button button1" name="Upvote" value="1">Upvote</button>
                <button type="submit" formmethod="post" class="button button2" name="Downvote" value="-1">Downvote</button>
                <input type="hidden" name="POST_ID" value="'.$postID.'"/>
                <input type="hidden" name="POSTER_ID" value="'.$currentUser.'"/>');
            echo('</form>');

            // Provide tools for user to comment
            echo('<form action="commentHandler.php" method="POST">');
            echo(' <textarea id="comment-area" name="comment-area" rows="4" cols="50">Leave a comment.</textarea><br>');
            echo(' <input type="submit" value="Submit Comment" />');
            echo(' <input type="hidden" name="POST_ID" value="'.$postID.'"/>');
            echo('</form>');

            // Allow moderators and higher to edit/delete ANY post
            if (getUserArrayFromCurrentUser($link)["PERMISSION_LEVEL"] >= 1)
            {
                
                echo('<form action="modPanel.php" method="POST">
                         <input type="hidden" name="Data" value="'.$postID.'"/>
                         <input type="submit" value="Edit in Mod Panel" />
                      </form>');
            }
            // Allow user to delete/edit their own post; we can just re-use the mod panel for this
            else if ($authorID == $currentUser)
            {
                echo('<form action="modPanel.php" method="POST">
                         <input type="hidden" name="Data" value="'.$postID.'"/>
                         <input type="submit" value="Edit Post" />
                      </form>');
            }
        }
    }

    return $numRows;
}

?>