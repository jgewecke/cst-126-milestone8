<!-- 
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website
-->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Search Menu</title>
	</head>
	<body>
        <form action="searchHandler.php" method="POST">
			<div class="group">
                <input type="text" name="Search"/>
				<input type="submit" name="SearchTitle" value="Search by Title" />
				<input type="submit" name="SearchBody" value="Search within Post" />
			</div>
	</body>
</html>

<?php

require_once './myfuncs.php';

// Connect to db
$link = dbConnect();

$sql = "SELECT TAG FROM posts";

$result = mysqli_query($link, $sql);

$row = $result->fetch_assoc();	// Read the Row from the Query

$array = array();
foreach($result as $i)
{
	if (in_array($i["TAG"], $array))
	{
		continue;
	}
	else
	{
		array_push($array, $i["TAG"]);
		echo('<input type="checkbox" checked name="Tag[]" value="'.$i["TAG"].'">
		<label for="'.$i["TAG"].'"> '.$i["TAG"].'</label><br>
 		');
	}

}
echo('</form> <a href="index.php">Main Menu</a>');

?>