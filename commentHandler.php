<a href="viewPosts.php">Return to posts.</a>

<?php
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp
// https://www.w3schools.com/sql/sql_delete.asp
// --NOTE--
// Post title is <=100 characters
// Post text is <= 2000 characters
// A chat filter is also added for some inappropriate words

    require_once('myfuncs.php');
    $link = dbConnect();
    
    $text = $_POST["comment-area"];
    $post_id = $_POST["POST_ID"]; 

    // Check for valid input /////////////////////////////////////////////////////////////
    // Simple filter
    $bannedWords = ['ass', 'fuck', 'bitch', 'cock', 'cum', 'cunt', 'dumbass', 'fag', 'faggot', 'goddamnit', 'goddamn', 'shit'];
    
    $approved = true;
    $textSizeLimit = 2000; // Number of characters allowed
            
    // Check for bad words
    foreach ($bannedWords as $badWord) {
        if (strpos(strtolower($text), $badWord) !== false) {
            echo("The word '" . $badWord . "' cannot be used in your text. Please remove it and try again. ");
            $approved = false;
        }
    }
    
    // Check for empty fields
    if ($text == NULL)  { echo "The text field cannot be blank.\n"; $approved = false; }
    
    // Check for appropriate lengths
    if (strlen($text) > $textSizeLimit) {
        echo "Your blog post is too big. Please make it 2000 characters or less";
        $approved = false;
    }
    
    // Is the post approved?
    if ($approved) {
        echo "The comment was posted! ";
    }
    else {
        echo "The comment was not posted. ";
        exit;
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    
    $current_user = getUserArrayFromCurrentUser($link);

    $sql = "INSERT INTO comments (POST_ID, USER_ID, FIRST_NAME, LAST_NAME, TEXT) VALUES ('$post_id','".$current_user['ID']."', '".$current_user['FIRST_NAME']."', '".$current_user['LAST_NAME']."', '$text')";
    mysqli_query($link, $sql);
?>