<style>
<?php require_once('style.css'); 
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp?>
</style>

<?php
require_once('myfuncs.php');

$link = dbConnect();
$currentUser = getUserId();
$sql = "SELECT * FROM posts";

get_posts($link, $sql);

// Close connection
mysqli_close($link);
?>

<a href="index.php">Return to Main Menu.</a>