<?php
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website

    require_once('myfuncs.php');

    forgetUserId();
    echo ('Logout Successful!');
    echo('</form> <a href="index.php">Main Menu</a>');
?>