<?php
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website
// References: https://exceptionshub.com/php-to-get-mysql-query-execution-time-in-query.html <- code to get execution time of my query request

require_once('myfuncs.php');

$link = dbConnect();
$numResults = 0;

// Input
$search = $_POST['Search'];
$tags = $_POST['Tag'];

// Adding the parenthesis to group the tags for an AND check with the query
$tag_query = '(';
for ($i = 0; $i < count($tags); $i++)
{
    $tag_query .= "TAG='$tags[$i]'";

    // Don't add for last iteration
    if ($i < count($tags)-1)
        $tag_query .= " OR ";
}
$tag_query .= ")";

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

// microtime to get my execution time
$msc = microtime(true);

// Search by title button was pressed
if (isset($_POST['SearchTitle']))
{
    // Add our selected tags to the query
    $sql = "SELECT * FROM posts WHERE TITLE LIKE '%$search%' AND " . $tag_query;
    $numResults = get_posts($link, $sql);

    // We found nothing so let's suggest posts with a related title
    if ($numResults == 0)
    {
        // Add our selected tags to the query
        $sql = "SELECT * FROM posts WHERE TEXT LIKE '%$search%' AND " . $tag_query;
        $numResults = get_posts($link, $sql);
        if ($numResults > 0)
        {
            echo "Couldn't find a post with that title, showing relevant results <br>";
        }
        else
        {
            echo "No posts were found! None of the keywords in the searchbar were found in the title or body of a post. <br>";
        }
    }
}

// Search by body button was pressed
if (isset($_POST['SearchBody']))
{
    // Add our selected tags to the query
    $sql = "SELECT * FROM posts WHERE TEXT LIKE '%$search%' AND " . $tag_query;
    $numResults = get_posts($link, $sql);

    // We found nothing so let's suggest posts with a related title
    if ($numResults == 0)
    {
        // Add our selected tags to the query
        $sql = "SELECT * FROM posts WHERE TITLE LIKE '%$search%' AND " . $tag_query;
        $numResults = get_posts($link, $sql);
        if ($numResults > 0)
        {
            echo "Couldn't find a post with that text in the body, showing relevant results <br>";
        }
        else
        {
            echo "No posts were found! None of the keywords in the searchbar were found in the title or body of a post. <br>";
        }
    }
}
$msc = microtime(true) - $msc;


echo "Number of posts found: " . $numResults . "<br>";
echo "Execution time: " . $msc . ' seconds<br>';
echo('</form> <a href="search.php">Go back</a>');
echo('</form> <a href="index.php">Main Menu</a>');

?>