<!-- 
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website

// TODO: Prevent Empty Tags
-->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>My Milestone 8 Application</title>
	</head>
	<body>
		<h2>My Milestone 8 Application</h2>
		<h3>Please login to make a post</h3>

		<div class="group">
			<a href="post.php">Create a Post</a>
		</div>
		<div class="group">
			<a href="viewPosts.php">View All Posts</a>
		</div>
		<div class="group">
			<a href="search.php">Search for Post</a>
		</div>
	</body>
</html>

<?php
	require_once('myfuncs.php');

	$link = dbConnect();


	echo ('<div class="group">');
			

	if (getUserId() == null)
	{
		echo('<a href="login.html">Login&nbsp;</a> ');
	}
	else
	{
		echo('<a href="logoutHandler.php">Sign Out&nbsp;</a> ');
	}
	echo ('<a href="register.html">Register</a></div>');

	// Allow Admins to see Admin panel
	if (getUserArrayFromCurrentUser($link)["PERMISSION_LEVEL"] == 2)
	{
		
		echo('<div class="group">
				<a href="adminPanel.php">Admin Panel</a>
			</div>');
	}
?>