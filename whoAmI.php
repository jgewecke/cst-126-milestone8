<?php
// Project Name: Milestone8
// Project Version: 1.7
// Module Name: Comment on and Rate a Blog Post
// Module Version: 1.7
// Programmer Name: Justin Gewecke
// Date: 8/2/2020
// Description: This module handles the rating and commenting portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once './myfuncs.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Who Am I</title>
</head>

<body>
 <h2>Hello My User ID Is: <?php echo " " . getUserId(); ?></h2><br>
 <a href="index.php">Go to main menu.</a>
</body>

</html>